import jwt from "jsonwebtoken";
import User from "../models/userModels.js";

const verifyToken = async (req, res, next) => {
  let token;
  try {
    if (
      req.headers.authorization &&
      req.headers.authorization.startsWith("Bearer")
    ) {
      token = req.headers.authorization.split(" ")[1];

      const decoded = jwt.verify(token, process.env.JWT_SECRET);

      req.user = await User.findById(decoded.userId).select("-password");
      req.tokenHead = token;

      next();
    } else if (!token) {
      res.status(401).json({ message: "Not authorized, no token" });
    } else {
      res
        .status(401)
        .json({ message: "Unknown error in verifyToken middleware" });
    }
  } catch (error) {
    console.error(error);
    res.status(401).json({ message: "Not authorized, token failed" });
  }
};

export { verifyToken };
