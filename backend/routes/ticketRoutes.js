import {
  createTicket,
  updateTicket,
  deleteTicket,
  getTicket,
  getAttachments,
  updateTicketPriority,
  getAssignedTickets,
} from "../controllers/ticketControllers.js";
import express from "express";
import { verifyToken } from "../middlewares/authMiddlewares.js";

const router = express.Router();

router.route("/new").post(createTicket);
router.route("/:id").get(verifyToken, getTicket);
router.route("/:id").put(verifyToken, updateTicket);
router.route("/:id").delete(verifyToken, deleteTicket);
router.route("/:id/assigned").get(verifyToken, getAssignedTickets);
router.route("/:id/priority").put(verifyToken, updateTicketPriority);
router.route("/:id/attachments").get(verifyToken, getAttachments);

export default router;
