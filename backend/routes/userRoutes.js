import express from "express";
import { verifyToken } from "../middlewares/authMiddlewares.js";
import {
  signup,
  login,
  updateNotifications,
  updateUserProfile,
  deleteUser,
  getUserProfile,
  getUserHistory,
  addAccountToOrg,
} from "../controllers/userControllers.js";

const router = express.Router();

router.route("/signup").post(signup);
router.route("/login").post(login);
router.route("/profile").get(verifyToken, getUserProfile);
router.route("/profile").put(verifyToken, updateUserProfile);
router.route("/profile/delete").delete(verifyToken, deleteUser);
router.route("/history/:id").get(verifyToken, getUserHistory);
router.route("/organization").post(verifyToken, addAccountToOrg);
router.route("/notifications").put(verifyToken, updateNotifications);

export default router;
