import mongoose from "mongoose";

const userSchema = new mongoose.Schema({
  username: {
    type: String,
    required: false,
  },
  email: {
    type: String,
    required: true,
    unique: true,
  },
  password: {
    type: String,
    required: true,
  },
  role: {
    type: String,
    enum: ["Client", "Agent"],
    default: "Client",
  },

  company: String,

  maxTickets: {
    type: Number,
    default: 50, // illimité en premium
  },

  activityHistory: {
    type: Number, // en mois
    default: 3, // 12 en premium
  },

  pushNotificationsLimit: {
    type: Number,
    default: 20, // 50 en premium
  },

  chatSupport: {
    type: Boolean,
    default: false, // true en premium
  },

  organization: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "Organization", // liens multi-org
  },
  plan: {
    type: String,
    enum: ["free", "premium"],
    default: "free",
  },
});

userSchema.virtual("id").get(function () {
  return this._id.toHexString();
});

userSchema.set("toJSON", {
  virtuals: true,
});

const User = mongoose.model("User", userSchema);

export default User;
