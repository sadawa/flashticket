import mongoose from "mongoose";

const ticketSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    description: {
      type: String,
      required: true,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    status: {
      type: String,
      enum: ["Open", "In Progress", "Closed"],
      default: "Open",
    },
    priority: {
      type: String,
      enum: ["Low", "Medium", "High"], // pour SLA différencié
      default: "Low",
    },

    type: {
      type: String,
      enum: ["Bug", "Incident", "Task"], // multiples catégories
    },

    attachments: [
      {
        name: String,
        sizeLimit: Number, // plus volumineux en premium
      },
    ],
    slaHours: {
      type: Number,
      default: 3, // Définissez une valeur par défaut selon vos besoins
    },

    notifications: [
      {
        type: String, // sms, email, push
        enabled: Boolean, // push instantanés en premium
      },
    ],
  },
  {
    timestamps: true,
  }
);

ticketSchema.virtual("id").get(function () {
  return this._id.toHexString();
});

ticketSchema.set("toJSON", {
  virtuals: true,
});

const Ticket = mongoose.model("Ticket", ticketSchema);

export default Ticket;
