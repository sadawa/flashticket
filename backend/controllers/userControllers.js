import User from "../models/userModels.js";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

// Inscription utilisateur
const signup = async (req, res) => {
  try {
    const { password } = req.body;
    const hashedPassword = bcrypt.hashSync(password);

    req.body.password = hashedPassword;

    const user = await User.create(req.body);

    res.status(201).json(user);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// Connexion utilisateur
const login = async (req, res) => {
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email });

    if (!user) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    const match = await bcrypt.compare(password, user.password);

    if (!match) {
      return res.status(400).json({ message: "Invalid credentials" });
    }

    // Générer un token JWT
    const token = jwt.sign({ userId: user._id }, process.env.JWT_SECRET, {
      expiresIn: "24h",
    });

    res.status(200).json({
      success: true,
      token,
    });
  } catch (error) {
    res.status(400).json({
      success: false,
      message: error.message,
    });
  }
};

// Récupérer profil utilisateur
const getUserProfile = async (req, res) => {
  try {
    const user = await User.findById(req.user.id);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    // Masquer des champs selon le rôle
    if (user.role === "client") {
      user.password = undefined;
      user.refreshToken = undefined;
    }

    res.json(user);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

// Mettre à jour profil utilisateur
const updateUserProfile = async (req, res) => {
  try {
    const user = await User.findByIdAndUpdate(
      req.user._id,
      { $set: req.body },
      { new: true }
    );

    res.json(user);
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

// Supprimer un utilisateur
const deleteUser = async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.user._id);

    if (!user) {
      return res.status(404).json({ message: "User not found" });
    }

    res.json({ message: "User deleted successfully" });
  } catch (error) {
    res.status(400).json({ message: error.message });
  }
};

const updateNotifications = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    if (user.plan === "free" && req.body.pushNotifications > 20) {
      return res.status(403).json({ message: "Notification limit reached" });
    }

    user.pushNotifications = req.body.pushNotifications;

    await user.save();

    res.json(user);
  } catch (error) {
    console.error(error);
  }
};

const getUserHistory = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    if (!user.plan) {
      return res.status(400).json({ message: "Invalid user plan" });
    }

    let limit = 3;

    if (user.plan === "premium") {
      limit = 12;
    }
    const history = await User.find({ _id: user._id })
      .limit(limit)
      .sort({ createdAt: -1 });

    res.json(history);
  } catch (error) {
    console.error(error);
  }
};

const addAccountToOrg = async (req, res) => {
  try {
    const { orgId } = req.params;

    if (!orgId) {
      return res.status(400).json({ message: "Organization ID required" });
    }

    const user = await User.findById(req.body.userId);

    user.organization = orgId;

    await user.save();

    res.json(user);
  } catch (error) {
    console.error(error);
  }
};

export {
  signup,
  login,
  updateUserProfile,
  deleteUser,
  getUserProfile,
  updateNotifications,
  getUserHistory,
  addAccountToOrg,
};
