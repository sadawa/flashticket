import Ticket from "../models/ticketModels.js";
import User from "../models/userModels.js";

const getTicket = async (req, res) => {
  try {
    const ticket = await Ticket.findById(req.params.id);
    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }
    res.json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const createTicket = async (req, res) => {
  try {
    const ticket = await Ticket.create(req.body);
    res.status(201).json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error", error: error.message });
  }
};

const updateTicket = async (req, res) => {
  try {
    const ticket = await Ticket.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
    });

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    res.json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const deleteTicket = async (req, res) => {
  try {
    const ticket = await Ticket.findByIdAndDelete(req.params.id);

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    res.json({ message: "Ticket deleted" });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const updateTicketPriority = async (req, res) => {
  try {
    const { priority, slaHours } = req.body;

    const ticket = await Ticket.findById(req.params.id);

    if (priority === "High") {
      ticket.slaHours = 8; // SLA raccourci en priorité haute
    } else {
      ticket.slaHours = slaHours; // SLA standard
    }

    ticket.priority = priority;

    await ticket.save();

    res.json(ticket);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const getAssignedTickets = async (req, res) => {
  try {
    const user = await User.findById(req.params.id);

    let limit = 5; // 5 max par défaut

    if (user.plan === "premium") {
      limit = 10; // 10 pour les premium
    }

    let query = { assignedAgent: user._id };

    const tickets = await Ticket.find(query).limit(limit);

    res.json(tickets);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const getAttachments = async (req, res) => {
  try {
    const ticket = await Ticket.findById(req.params.id).populate("user"); // populate 'user' field

    if (!ticket) {
      return res.status(404).json({ message: "Ticket not found" });
    }

    if (ticket.user.plan === "premium") {
      return res.json(ticket.attachments);
    }

    // Otherwise, limit attachment size
    const limitedAttachments = ticket.attachments.filter(
      (attach) => attach.size < 2000000
    );

    res.json(limitedAttachments);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: "Server error" });
  }
};

const addTicketCategory = async (req, res) => {
  if (req.body.category && req.body.category !== "Other") {
    return res
      .status(403)
      .json({ message: "Category not allowed on this plan" });
  }

  // Ajouter la catégorie autrement

  const ticket = await Ticket.findByIdAndUpdate(req.params.id, {
    category: req.body.category,
  });

  res.json(ticket);
};

export {
  getTicket,
  createTicket,
  updateTicket,
  deleteTicket,
  updateTicketPriority,
  getAssignedTickets,
  getAttachments,
  addTicketCategory,
};
