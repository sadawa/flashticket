import express from "express";
import dotenv from "dotenv";
import colors from "colors";
import connectDB from "./config/db.js";
import cors from "cors";
import bodyParser from "body-parser";
import morgan from "morgan";
import helmet from "helmet";

// Configuration ENV
dotenv.config();

//Connect to db
connectDB();

//Middleware
const app = express();
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
app.options("*", cors());
app.use(helmet());

//Import Routes
import ticketRoutes from "./routes/ticketRoutes.js";
import userRoutes from "./routes/userRoutes.js";

//Route api
app.use("/api/users", userRoutes);
app.use("/api/tickets", ticketRoutes);

// Development mode
if (process.env.NODE_ENV === "development") {
  const __dirname = path.resolve();
  app.use(
    "backend/public",
    express.static(path.join(__dirname, "/backend/public"))
  );
  //   app.get("*", (req, res) =>
  //   res.sendFile(path.resolve(__dirname, "backend", "public", "index.html"))
  // );
  app.use(morgan("dev"));
}

const PORT = process.env.PORT || 5000;

app.listen(
  PORT,
  console.log(`Server runnig in ${PORT} mode on port ${PORT}`.yellow.bold)
);
